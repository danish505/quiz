<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class FormController extends Controller
{
    //

	public function formsubmit(Request $request)
	{
		 $validatedData = $request->validate([
        'email' => 'required',
        'name' => 'required',
        'q1' => 'required',
        'q2' => 'required',
        'q3' => 'required',
        'q4' => 'required',
        'q5' => 'required',
        'q6' => 'required',
        'q7' => 'required',
        'q8' => 'required',
        'q9' => 'required',
        'q10' => 'required',
    	]);

		$arr['name'] = $request->input('name');
		$arr['email'] = $request->input('email');
		$arr['q1'] = $request->input('q1');
		$arr['q2'] = $request->input('q2');
		$arr['q3'] = $request->input('q3');
		$arr['q4'] = $request->input('q4');
		$arr['q5'] = $request->input('q5');
		$arr['q6'] = $request->input('q6');
		$arr['q7'] = $request->input('q7');
		$arr['q8'] = $request->input('q8');
		$arr['q9'] = $request->input('q9');
		$arr['q10'] = $request->input('q10');
 
		DB::table('quest')->insert([
			'unrelated_thoughts'=>$arr['q1'],
			'mental_effort'=>$arr['q2'],
			'trouble_listening'=>$arr['q3'],
			'organizing'=>$arr['q4'],
			'attention_details'=>$arr['q5'],
			'forget_something'=>$arr['q6'],
			'misplace'=>$arr['q7'],
			'unable_play'=>$arr['q8'],
			'difficulty_waiting'=>$arr['q9'],
			'leave_seat'=>$arr['q10'],
			'name'=>$arr['name'],
			'email'=>$arr['email']
		]);

 		return redirect('/');
	}
 

}
