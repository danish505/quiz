<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $quest = Quest::all();

        return view('home',['quest'=>$quest]);
    }
}
