<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quest', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unrelated_thoughts');
            $table->string('mental_effort');
            $table->string('trouble_listening');
            $table->string('organizing');
            $table->string('attention_details');
            $table->string('forget_something');
            $table->string('misplace');
            $table->string('unable_play');
            $table->string('difficulty_waiting');
            $table->string('leave_seat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quest');
    }
}
