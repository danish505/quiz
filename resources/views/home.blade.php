@extends('layouts.app')

@section('content')
<div class="container text-center">
    <div class="jumbotron">
        <h3>Quiz results</h3>
    </div>
                  
                     @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered">
                        <thead>
                            <th>Question 1</th>
                            <th>Question 2</th>
                            <th>Question 3</th>
                            <th>Question 4</th>
                            <th>Question 5</th>
                            <th>Question 6</th>
                            <th>Question 7</th>
                            <th>Question 8</th>
                            <th>Question 9</th>
                            <th>Question 10</th>
                        </thead>
                    @foreach($quest as $key)
                    <tr>
                        <td>{{ $key->name}}</td>
                        <td>{{ $key->email}}</td>
                        <td>{{ $key->unrelated_thoughts}}</td>
                        <td>{{ $key->mental_effort}}</td>
                        <td>{{ $key->trouble_listening}}</td>
                        <td>{{ $key->organizing}}</td>
                        <td>{{ $key->attention_details}}</td>
                        <td>{{ $key->forget_something}}</td>
                        <td>{{ $key->unable_play}}</td>
                        <td>{{ $key->leave_seat}}</td>
                    </tr>
                    @endforeach
                </table>

                    
                </div>
  
@endsection
