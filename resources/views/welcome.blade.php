<html>
    <head>
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}"/>
    </head>
    <body>
    
    <div class="container">
    <div class="jumbotron">
        <h1>Quiz</h1>
    </div>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('formsubmit')}}" method="post">
  @csrf
  <div class="row">
  <div class="form-group col-md-6">
    <label class="font-weight-bold" for="email">Email address:</label class="font-weight-bold">
    <input type="text" class="form-control" name="email" id="email" required>
  </div>
  <div class="form-group col-md-6">
<label class="font-weight-bold" for="name">Full Name</label class="font-weight-bold">
    <input type="text" class="form-control" name="name" id="name" required>
  </div>
</div>

<div class="col-md-12 form-group">
<label class="font-weight-bold" >How often are you easily distracted by external stimuli, like something in your environment or unrelated thoughts?</label class="font-weight-bold">
<br/>
<input type="radio" name="q1" value="never" >Never
<div class="radio"><input type="radio" required name="q1" value="rarely">Rarely</div>
<div class="radio"><input type="radio" name="q1" value="sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q1" value="often">Often</div>
</div>
<div class="col-md-12 form-group">
<label class="font-weight-bold">How often do you avoid, dislike, or are reluctant to engage in tasks that require sustained mental effort or thought?</label class="font-weight-bold">
<br/>
<input type="radio"  name="q2" value="never">Never
<div class="radio"><input type="radio" required name="q2" value="rarely">Rarely</div>
<div class="radio"><input type="radio" name="q2" value="sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q2" value="often">Often</div>
</div>
<div class="col-md-12 form-group">
<label class="font-weight-bold">How often do you have trouble listening to someone, even when they are speaking directly to you — like your mind is somewhere else?</label class="font-weight-bold"><br/>
<input type="radio" name="q3" value="never">Never
<div class="radio"><input type="radio" required name="q3" value="Rarely">Rarely</div>
<div class="radio"><input type="radio" name="q3" value="Sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q3" value="Often">Often</div>
</div>
<div class="col-md-12 form-group">
<label class="font-weight-bold"> How often do you have difficulty in organizing an activity or task needing to get done (e.g., poor time management, fails to meet deadlines, difficulty managing sequential tasks)?
</label class="font-weight-bold"><br/>
<div><input type="radio"  required name="q4" value="Never">Never</div>
<div class="radio"><input type="radio"  name="q4" value="Rarely">Rarely</div>
<div class="radio"><input type="radio" name="q4" value="Sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q4" value="Often">Often</div>
</div>
<div class="col-md-12 form-group ">
<label class="font-weight-bold">How often do you fail to give close attention to details, or make careless mistakes in things such as schoolwork, at work, or during other activities?
</label class="font-weight-bold"><br/>
<input type="radio"  name="q5" value="Never">Never
<div class="radio"><input type="radio" required name="q5" value="Rarely">Rarely</div>
<div class="radio"><input type="radio" name="q5" value="Sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q5" value="Often">Often</div>
</div>
<div class="col-md-12 form-group">
<label class="font-weight-bold">How often do you forget to do something you do all the time, such as missing an appointment or paying a bill?</label class="font-weight-bold"><br/>
<input type="radio" required name="q6" value="Never">Never
<div class="radio"><input type="radio" name="q6" value="Rarely">Rarely</div>
<div class="radio"><input type="radio" name="q6" value="Sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q6" value="Often">Often</div>
</div>
<div class="col-md-12 form-group">
<label class="font-weight-bold">How often do you lose, misplace or damage something that's necessary in order to get things done (e.g., your phone, eyeglasses, paperwork, wallet, keys, etc.)?</label class="font-weight-bold"><br/>
<input type="radio"  name="q7" value="Never">Never
<div class="radio"><input type="radio" required name="q7" value="Rarely">Rarely</div>
<div class="radio"><input type="radio" name="q7" value="Sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q7" value="Often">Often</div>
</div>
<div class="col-md-12 form-group">
<label class="font-weight-bold">How often are you unable to play or engage in leisurely activities quietly?</label class="font-weight-bold"><br/>
<input type="radio" required name="q8" value="Never">Never
<div class="radio"><input type="radio" name="q8" value="Rarely">Rarely</div>
<div class="radio"><input type="radio" name="q8" value="Sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q8" value="Often">Often</div>
</div>
<div class="col-md-12 form-group">
<label class="font-weight-bold"> How often do you have difficulty waiting your turn, such as while waiting in line?</label class="font-weight-bold"><br/>
<input type="radio" name="q9" value="Never">Never
<div class="radio"><input type="radio" name="q9" value="Rarely">Rarely</div>
<div class="radio"><input type="radio" required name="q9" value="Sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q9" value="Often">Often</div>
</div>
<div class="col-md-12 form-group">
<label class="font-weight-bold"> How often do you leave your seat in situations when remaining seated is expected (e.g., leaving your place in the office or workplace)?</label class="font-weight-bold"><br/>
<input type="radio" name="q10" value="Never">Never
<div class="radio"><input type="radio" required name="q10" value="Rarely">Rarely</div>
<div class="radio"><input type="radio" name="q10" value="Sometimes">Sometimes</div>
<div class="radio"><input type="radio" name="q10" value="Often">Often</div>
</div>

<input type="submit" class="btn btn-primary"/>

  </form>
    </div>
     <script src="{{ asset('js/jquery.js')}}"></script>
     <script src="{{ asset('js/bootstrap.min.js')}}"></script>
     <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
    </body>
</html>